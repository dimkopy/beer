from django.apps import AppConfig


class BeerPagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'beer_pages'
